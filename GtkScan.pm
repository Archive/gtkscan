#!/usr/bin/perl -w
#
# gtkscan - Scan GTK/GNOME headers and stick all the info in a big hash
#  Copyright (C) 2000 Red Hat, Inc.
#  developed by Havoc Pennington <hp@redhat.com>
# based on:
#  gtk-doc - GTK DocBook documentation generator.
#  Copyright (C) 1998  Damon Chaplin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#

#############################################################################
# Module:       GtkScan.pm
# Description : Extracts declarations of functions, macros, enums, structs
#		and unions from header files.
#
#               Scanned info is stuffed into hashes
#############################################################################

use strict;

package GtkScan;
1;

### External data types

## An Object is a hash with these keys:
#    'name' => string name of the object, e.g. 'GtkObject'
#    'fields' => list of lists, each sub-list contains a type name and a field name
#                e.g. ('int', 'flags')
#                these are public struct fields that may need accessors
#    'methods' => list of functions (each is also stored in the "functions" hash)
#                 associated with the object (should be methods of the object)
#    'parent' => string name of the object's parent, if it has one,
#                otherwise unset
#    'args'   => list of Arg
#    'signals' => list of Signal

## An Arg is a hash with these keys:
#    'name' => such as "x_width", "padding"
#    'type' => string, like
#    'flags' => flags string:

## A Signal is a hash with these keys:
#    'name' => signal name, "clicked"
#    'args' => signal args, as for Function
#    'returns' => string name, as for Function

## A Function is a hash with these keys:
#    'name' => string name of the function, e.g. 'gtk_widget_show'
#    'returns' => string name of the return type, e.g. 'gint' or 'void'
#    'args' => list of lists, each sub-list is a type/varname pair.
#              e.g. (('GtkWidget*', 'child'), ('gint', 'padding'))
#              varargs are represented as ('varargs', '')
# (private)
#    'unparsed_args' => unparsed contents of the parentheses after the function name

## A UserFunction is just like a function, but the name is the typedef'd name

## A Macro is a hash with these keys:
#    'name' => string name of macro, GTK_BLAH_BLAH
#    'args' => list of strings, each string is a parameter name

## A Struct is a hash with these keys:
# 'name' => name of the struct
# 'ref_func' => func to ref/copy the struct, may be undefined
# 'unref_func' => func to unref/free the struct, may be undefined

## A Union is a hash with these keys:
# 'name' => name of the union

## A Typedef is hash with these keys:
# 'name' => new type alias name
# 'original' => original name
# structs/unions aren't counted as typedefs

## A Variable is a hash with these keys:
# 'name' => variable name
# 'type' => variable type

## An Enum is a hash with these keys:
# 'name' => enum name
# 'elems' => enum element names (list of strings)
# 'nicks' => hash from enum element names to "nicks" (non-prefix part like
#            "same-app" for GTK_TARGET_SAME_APP)

### All the types can have a "declaration" field giving the complete declaration,
### such as "typedef guint GtkType;" or "struct _Foo { int blah; };"

###########################
###### Public API
###########################

sub new {
  my $pkgname = shift;
  my ($module) = @_;

  my $self = {};

  $self->{module} = $module;

  $self->{complete} = 0;
  $self->{scanned_dir} = 0;

  $self->{variables} = ();
  $self->{user_functions} = ();
  $self->{unions} = ();
  $self->{objects} = ();
  $self->{typedefs} = ();
  $self->{macros} = ();
  $self->{enums} = ();
  $self->{functions} = ();
  $self->{structs} = ();

  bless $self, $pkgname;

  return $self;
}

sub scan_directory {
  my $self = shift;
  my $dirname = shift;

  my $ignore_headers = "";

  my $option = shift;
  while (defined($option)) {
    if ($option eq "exclude_files") {
      $ignore_headers = shift;
    }
    $option = shift;
  }

  $self->{scanned_dir} = 1;

  &ScanHeaders ($self, $dirname, $ignore_headers);
}

sub scan_file {
  my $self = shift;
  my ($filename) = @_;

  $self->{scanned_dir} = 1;

  &ScanHeader ($self, $filename, "");
}

sub extract_types {
  my $self = shift;
  my ($typesfile) = @_;

  $self->{scanned_dir} || die "Must scan at least one dir or file before you can scan types\n";
  $self->{complete} || die "Must call scanning_complete() before extracting types\n";

  # stupid hack
  my $need_gnome_init = 0;
  if ($self->{module} eq 'gnome' ||
      $self->{module} eq 'zvt') {
    $need_gnome_init = 1;
  }

  &QueryObjectRuntime($typesfile, 0, $need_gnome_init,
                      $self->{objects}, $self->{module});
}

sub scanning_complete {
  my $self = shift;

  # These cases are really annoying and aren't handled so far:
  # typedef struct _GdkWindow	      GdkPixmap;
  # typedef struct _GdkWindow	      GdkBitmap;
  # typedef struct _GdkWindow	      GdkDrawable;
  # typedef struct _GtkEnumValue      GtkFlagValue;
  # So, insert the following hack...

  if (defined($self->{structs}->{'GdkWindow'})) {
    my (%struct) = ('name' => 'GdkPixmap',
                    'declaration' => '');
    $self->{structs}->{'GdkPixmap'} = \%struct;

    my (%struct2) = ('name' => 'GdkBitmap',
                     'declaration' => '');
    $self->{structs}->{'GdkBitmap'} = \%struct2;

    my (%struct3) = ('name' => 'GdkDrawable',
                    'declaration' => '');
    $self->{structs}->{'GdkDrawable'} = \%struct3;

    my (%struct4) = ('name' => 'GtkFlagValue',
                    'declaration' => '');
    $self->{structs}->{'GtkFlagValue'} = \%struct4;
  }

  &FixupObjectsInStructs ($self->{objects}, $self->{structs});
  &FixupObjectsInTypedefs ($self->{objects}, $self->{typedefs});
  &FixupStructsInTypedefs ($self->{structs}, $self->{typedefs});
  &FixupStructFuncs($self->{structs}, $self->{functions});
  &FindFieldsForObjects ($self->{objects});
  &FixupFunctionArgs ($self->{functions});
  &FixupFunctionArgs ($self->{user_functions});
  &FixupMacroArgs ($self->{macros});
  &FixupEnumElems ($self->{enums});

  $self->{complete} = 1;
}

sub get_objects {
  my $self = shift;
  $self->{complete} || die "must call scanning_complete() before using results\n";

  return $self->{objects};
}

sub spew_objects {
  my $self = shift;
  $self->{complete} || die "must call scanning_complete() before using results\n";

  &SpewObjects($self->{objects});
}

my %shouldbeconst = (
                     'gdk_fontset_load' => '1',
                     'gdk_gc_set_dashes' => '3',
                     'gdk_string_to_compound_text' => '1',
                     'gdk_window_set_icon_name' => '2',
                     'gtk_widget_set_composite_name' => '2',
                     'gtk_item_factory_print_func' => '2',
                     'gtk_progress_set_format_string' => '2',
                     'gtk_tree_item_new_with_label' => '1',
                     'gtk_paint_arrow' => '7',
                     'gtk_paint_box' => '7',
                     'gtk_paint_box_gap' => '7',
                     'gtk_paint_check' => '7',
                     'gtk_paint_cross' => '7',
                     'gtk_paint_diamond' => '7',
                     'gtk_paint_extension' => '7',
                     'gtk_paint_flat_box' => '7',
                     'gtk_paint_focus' => '5',
                     'gtk_paint_handle' => '7',
                     'gtk_paint_hline' => '6',
                     'gtk_paint_option' => '7',
                     'gtk_paint_oval' => '7',
                     'gtk_paint_polygon' => '7',
                     'gtk_paint_ramp' => '7',
                     'gtk_paint_shadow' => '7',
                     'gtk_paint_shadow_gap' => '7',
                     'gtk_paint_slider' => '7',
                     'gtk_paint_string' => '6',
                     'gtk_paint_tab' => '7',
                     'gtk_paint_vline' => '6',
                     'gtk_theme_engine_get' => '1'
                    );

sub write_defs_file {
  my $self = shift;
  my ($outfile) = @_;

  $self->{complete} || die "must call scanning_complete() before using results\n";

  open (OUTPUT, ">$outfile") || die "can't open output file $outfile: $!\n";

  print OUTPUT ";; -*- scheme -*-\n;;\n";
  print OUTPUT ";; Definitions file automatically generated on: " . `date`;
  print OUTPUT ";;\n\n";

  print OUTPUT ";;;;;;;;; FUNCTIONS\n\n";

  my $functions = $self->{functions};
  my $fname;
  foreach $fname (sort keys %$functions) {
    my $f = $functions->{$fname};

    print OUTPUT "(define-func $fname\n";

    print OUTPUT "  ";
    print OUTPUT &CanonicalTypename($self, $f->{returns});

    my $args = $f->{args};
    if ($args) {
      print OUTPUT "\n  (";
      my $argnum = 1;
      my $a;
      foreach $a (@$args) {
        if ($argnum != 1) {
          print OUTPUT "\n   ";
        }

        my $argtype = &CanonicalTypename($self, $a->[0]);

        if (defined($shouldbeconst{$fname})) {
            my $whicharg = $shouldbeconst{$fname};
            if ($whicharg == $argnum) {
                $argtype =~ s/string/shouldbeconst_string/;
            }
        }

        print OUTPUT "(" . $argtype . " " . $a->[1] . ")";

        $argnum += 1;
      }
      print OUTPUT ")";
    }
    print OUTPUT ")\n\n";
  }

  print OUTPUT ";;;;;;;;; FUNCTION TYPEDEFS\n\n";

  $functions = $self->{user_functions};
  foreach $fname (sort keys %$functions) {
    my $f = $functions->{$fname};

    print OUTPUT "(define-func-type $fname\n";

    print OUTPUT "  ";
    print OUTPUT &CanonicalTypename($self, $f->{returns});

    my $args = $f->{args};
    if ($args) {
      print OUTPUT "\n  (";
      my $first = 1;
      my $a;
      foreach $a (@$args) {
        if ($first) {
          $first = 0;
        } else {
          print OUTPUT "\n   ";
        }

        print OUTPUT "(" . &CanonicalTypename($self, $a->[0]) . " " . $a->[1] . ")";
      }
      print OUTPUT ")";
    }
    print OUTPUT ")\n\n";
  }

  print OUTPUT "\n;;;;;;;;; STRUCTS\n\n";

  my $structs = $self->{structs};

  my $key;
  foreach $key (sort keys %$structs) {
    my $s = $structs->{$key};
    my $name = $s->{name};

    my $ref_func = "";
    my $unref_func = "";

    print OUTPUT "(define-boxed $name\n  $ref_func\n  $unref_func)\n\n";
  }

  print OUTPUT "\n;;;;;;;;; UNIONS\n\n";

  my $unions = $self->{unions};

  foreach $key (sort keys %$unions) {
    my $s = $unions->{$key};
    my $name = $s->{name};

    my $ref_func = "";
    my $unref_func = "";

    print OUTPUT "(define-boxed $name\n  $ref_func\n  $unref_func)\n\n";
  }

  print OUTPUT "\n;;;;;;;;; TYPEDEFS\n\n";

  my $typedefs = $self->{typedefs};

  foreach $key (sort keys %$typedefs) {
    my $s = $typedefs->{$key};
    my $name = $s->{name};
    my $orig = $s->{original};

    if (defined($name)) {
      if (defined($orig)) {
        $orig = &CanonicalTypename($self, $orig);
        print OUTPUT "(define-type-alias $orig $name)\n\n";
      } else {
        print "WARNING: typedef $name has no original type\n";
      }
    } else {
      print "WARNING: typedef with no name field?\n";
    }
  }

  print OUTPUT "\n;;;;;;;;; OBJECTS\n\n";

  my $objects = $self->{objects};

  foreach $key (sort keys %$objects) {
    my $o = $objects->{$key};
    my $name = $o->{name};
    my $decl = $o->{declaration};
    my $fields = $o->{fields};
    my $parent = $o->{parent};
    my $signals = $o->{signals};
    my $args = $o->{args};

    if (!defined($parent)) {
      $parent = "nil";
    }

    print OUTPUT "(define-object $name ($parent))\n\n";
  }

  print OUTPUT ";;;;;;;;; ENUMERATIONS\n\n";

  my $enums = $self->{enums};

  foreach $key (sort keys %$enums) {
        my $e = $enums->{$key};
        my $name = $e->{name};
        my $elems = $e->{elems};
        my $decl = $e->{declaration};

        # if the declaration has bitshifts decide it's a flags?
        my $flags = "enum";
        if ($e->{is_flags}) {
          $flags = "flags";
        }

        print OUTPUT "(define-$flags $name\n";

        my $prefix;
        my $el;
        foreach $el (@$elems) {
          if (defined $prefix) {
            my $tmp = ~ ($el ^ $prefix);
            ($tmp) = $tmp =~ /(^\xff*)/;
            $prefix = $prefix & $tmp;
          } else {
            $prefix = $el;
          }
        }

        if (!defined($prefix)) {
          print "WARNING: no prefix for enum $name, probably has no elements\n";
          print OUTPUT "()";
        } else {
          # Trim so that it ends in an underscore
          $prefix =~ s/_[^_]*$/_/;

          foreach $el (@$elems) {
              print OUTPUT "   ";

            my $nick;

            # check for explicit nick
            $nick = $e->{nicks}->{$el};

            if (!defined($nick)) {
              $nick = $el;
              $nick =~ s/$prefix//;
              $nick = lc($nick);
              $nick =~ s/_/-/g;
            }

            print OUTPUT "(" . $nick . " " . $el . ")\n";
          }
        }
        print OUTPUT ")\n\n";
  }
}

#################################
###### PRIVATE STUFF PAST HERE
#################################

my %typeconv = ( 'guint' => 'uint',
                 'gint'  => 'int',
                 'gfloat'  => 'float',
                 'gdouble'  => 'double',
                 'gint*'  => 'int*',
                 'gboolean' => 'bool',
                 'gchar*' => 'string',
                 'const_gchar*' => 'const_string',
                 'const_char*' => 'const_string',
                 'glong' => 'long',
                 'gulong' => 'ulong',
                 'gint32' => 'int32',
                 'guint32' => 'uint32',
                 'guint32*' => 'uint32*',
                 'guint32**' => 'uint32**',
                 'gint16' => 'int16',
                 'guint16' => 'uint16',
                 'guchar' => 'uchar',
                 'guchar*' => 'uchar*',
                 'gshort' => 'short',
                 'gushort' => 'ushort',
                 'gushort*' => 'ushort*',
                 'void' => 'none',
                 'gpointer' => 'void*',
                 'gchar' => 'char',
                 'unsigned int' => 'uint',
                 'unsigned char' => 'uchar',
                 'gulong*' => 'ulong*',
                 'gboolean*' => 'bool*',
                 'guint8' => 'uint8',
                 'gint8' => 'int8',
                 'guint8*' => 'uint8*',
                 'gint8*' => 'int8*',
                 'gdouble*' => 'double*',
                 'guint*' => 'uint*',
                 'gfloat*' => 'float*'
               );

sub StripSpaces {
  my ($str) = @_;

  $$str =~ s/^\s*//;
  $$str =~ s/\s*$//;
}

sub CanonicalTypename {
    my $self = shift;
    my ($type) = @_;

    &StripSpaces(\$type);

    # spaces become underscore
    $type =~ s/\s/_/g;

    my $conv = $typeconv{$type};

    if (defined($conv)) {
      return $conv;
    }

    # Strip * off of boxed and object type names
    if ($type =~ /^([A-Za-z0-9_]+)\*$/) {
      if ($self->{objects}->{$1}) {
        return $1;
      } elsif ($self->{structs}->{$1}) {
        return $1;
      } elsif ($self->{unions}->{$1}) {
        return $1;
      }
    }

    return $type;
}

################################################################################
########### Post-scan fixup and additional parsing
################################################################################

sub CleanupTypename {
  my ($typenameref) = @_;

  # for now this basically just strips extraneous whitespace

  if ($$typenameref =~ m/^
	    (const\s+|unsigned\s+)*(struct\s+)? # mod1
	    (\w+)\s*                            # type
	    (\**)\s*                            # ptr1
	    (const\s+)?                         # mod2
	    (\**)?\s*                           # ptr2
	               $/x) {
    my $mod1 = defined($1) ? $1 : "";
    if (defined($2)) { $mod1 .= $2; }
    my $type = $3;
    my $ptr1 = $4;
    my $mod2 = defined($5) ? $5 : "";
    my $ptr2 = $6;

    $$typenameref = "$mod1$type$ptr1$mod2$ptr2";
  } else {
    print "WARNING: couldn't cleanup type: $$typenameref\n";
  }
}

# Remove GtkObject subclasses from the hash of structs,
# first setting the declaration field of the objects
sub FixupObjectsInStructs {
  my ($objs, $structs) = @_;

  my $o;
  foreach $o (keys %$objs) {
    if (defined $$structs{$o}) {
      $objs->{$o}->{declaration} = $structs->{$o}->{declaration};
      delete $$structs{$o};
      my $klass = $o . "Class";
      delete $$structs{$klass};
    }
  }
}

sub FixupObjectsInTypedefs {
  my ($objs, $typedefs) = @_;

  my $o;
  foreach $o (keys %$objs) {
    if (defined $$typedefs{$o}) {
      delete $$typedefs{$o};
      my $klass = $o . "Class";
      delete $$typedefs{$klass};
    }
  }
}

# Remove structs from the typedefs
sub FixupStructsInTypedefs {
  my ($structs, $typedefs) = @_;

  my $s;
  foreach $s (keys %$structs) {
    if (defined $$typedefs{$s}) {
      delete $$typedefs{$s};
    }
  }
}

sub FixupStructFuncs {
  my ($structs, $functions) = @_;

  my $name;
  foreach $name (keys %$structs) {
    my $s = $structs->{$name};

      #### FIXME
  }
}

sub FindFieldsForObjects {
  my ($objs) = @_;

  my $objname;
  foreach $objname (keys %$objs) {
    my $o = $objs->{$objname};
    my @fields;
    my $dec = $o->{declaration};
    if ($dec) {
      my @vardecls = &ParseStructDeclaration($dec, 1);

      # vardecls is a list of full variable declarations, like "guint blah"
      # excluding "private" fields

      my $d;
      foreach $d (@vardecls) {
        my @pair = &ParseSingleDeclaration($d);
        push @fields, \@pair;
      }
    }
    $o->{fields} = \@fields;
  }
}

#############################################################################
# Function    : ParseStructDeclaration
# Description : This function takes a structure declaration and
#               breaks it into individual type declarations.
# Arguments   : $declaration - the declaration to parse
#               $is_object - true if this is an object structure
#               $typefunc - function reference to apply to type
#               $namefunc - function reference to apply to name
#############################################################################

sub ParseStructDeclaration {
    my ($declaration, $is_object, $typefunc, $namefunc) = @_;

    # Remove all private parts of the declaration

    # For objects, assume private
    if ($is_object) {
	$declaration =~ s!(struct\s+\w*\s*\{)
	                  .*?
			  (?:/\*\s*<\s*public\s*>\s*\*/|(?=\}))!$1!msgx;
    }

    $declaration =~ s!\n?[ \t]*/\*\s*<\s*private\s*>\s*\*/
	              .*?
		      (?:/\*\s*<\s*public\s*>\s*\*/|(?=\}))!!msgx;

    # Remove all other comments;
    $declaration =~ s@/\*([^*]+|\*(?!/))*\*/@ @g;

    my @result = ();

    if ($declaration =~ /^\s*$/) {
	return @result;
    }

    # Prime match after "struct {" declaration
    if (!scalar($declaration =~ m/struct\s+\w*\s*\{/msg)) {
      return @result;
      die "Structure declaration '$declaration' does not begin with struct [NAME] {\n";
    }

    # Treat lines in sequence, allowing singly nested anonymous structs
    # and unions.
    while ($declaration =~ m/\s*([^{;]+(\{[^\}]*\}[^{;]+)?);/msg) {
	my $line = $1;

	last if $line =~ /^\s*\}\s*\w*\s*$/;

	# FIXME: Just ignore nested structs and unions for now
	next if $line =~ /{/;

	# FIXME: The regexes here are the same as in OutputFunction;
	#        this functionality should be separated out.

	if ($line =~ m/^
	    (const\s+|unsigned\s+)*(struct\s+)? # mod1
	    (\w+)\s*                            # type
	    (\**)\s*                            # ptr1
	    (const\s+)?                         # mod2
	    (\**)?\s*                           # ptr2
	    (\w+(?:\s*,\s*\w+)*)\s*             # name
	    (?:((?:\[[^\]]*\]\s*)+) |           # array
	       (:\s*\d+))?\s*                   # bits
	               $/x) {
	    my $mod1 = defined($1) ? $1 : "";
	    if (defined($2)) { $mod1 .= $2; }
	    my $type = $3;
	    my $ptr1 = $4;
	    my $mod2 = defined($5) ? $5 : "";
	    my $ptr2 = $6;
	    my $name = $7;
	    $ptr1 = " " . $ptr1;
	    my $array = defined($8) ? $8 : "";
	    my $bits =  defined($9) ? " $9" : "";
	    my $ptype = defined $typefunc ? $typefunc->($type) : $type;

	    # FIXME:
	    # As a hack, we allow the "name" to be of the form
	    # "a, b, c". This isn't the correct C syntax, but
	    # at least we get "gint16 x, y" right. Such constructs
	    # should really be completely removed from the source.
	    # Or we should really try to understand the C syntax
	    # here...

	    my @names = split /\s*,\s*/, $name;
	    for my $n (@names) {
		if (defined $namefunc) {
		    $n = $namefunc->($n);
		}
		push @result, "$mod1$ptype$ptr1$mod2$ptr2$n$array$bits";
	    }

        # Try to match structure members which are functions
	} elsif ($line =~ m/^
		 (const\s+|unsigned\s+)*(struct\s+)?  # mod1
		 (\w+)\s*                             # type
		 (\**)\s*                             # ptr1
		 (const\s+)?                          # mod2
		 \(\s*\*\s*(\w+)\s*\)\s*              # name
		 \(([^)]*)\)\s*                       # func_params
		            $/x) {

	    my $mod1 = defined($1) ? $1 : "";
	    if (defined($2)) { $mod1 .= $2; }
	    my $type = $3;
	    my $ptr1 = $4;
	    my $mod2 = defined($5) ? $5 : "";
	    my $name = $6;
	    my $func_params = $7;
	    my $ptype = defined $typefunc ? $typefunc->($type) : $type;
	    my $pname = defined $namefunc ? $namefunc->($name) : $name;

	    push @result, "$mod1$ptype$ptr1$mod2 (*$pname) ($func_params)";

	} else {
	    warn "Cannot parse structure field $line";
	}
    }

    return @result;
}

sub ParseSingleDeclaration {
  my ($declaration) = @_;

  if ($declaration =~ /^((const\s+|unsigned\s+)*(struct\s+)?(\w+)\s*(\**)\s*(const\s+)?(\**))\s*(\w+)?\s*(\[\d*\])?\s*[,\n]?/) {

    my $type;
    if (defined ($1)) {
      $type = $1;
      if (defined ($9)) {
        # [] array brackets after variable name, add a *
        $type .= "*";
      }
    } else {
      print "WARNING: parse failed on type for declaration: $declaration\n";
      $type = "FIXMEFIXMEPARSEERROR";
    }

    my $name;
    if (defined ($8)) {
      $name = $8;
    } else {
      print "WARNING: no name found for declaration: $declaration\n";
      $name = "FIXME";
    }

    return ($type, $name);
  } else {
    print "WARNING: can't parse struct field declaration: $declaration\n";
    return ("FIXME","FIXME");
  }
}

# Remove structs/objects from typedefs
# (function is unused, not shown to be needed yet)
sub FixupStuffInTypedefs {
  my ($typedefs, $objs, $structs) = @_;

  my $o;
  foreach $o (keys %$objs) {
    delete $$typedefs{$o};
    my $klass = $o . "Class";
    delete $$typedefs{$klass};
  }

  my $s;
  foreach $s (keys %$objs) {
    delete $$typedefs{$s};
  }
}

# fill in the enum elements
sub FixupEnumElems {
  my ($enums) = @_;

  my $enumname;
  foreach $enumname (keys %$enums) {
    my $e = $enums->{$enumname};

    my $declaration = $e->{declaration};

    my @elems = ParseEnumDeclaration($e, $declaration);

    $e->{elems} = \@elems;
  }
}

sub FixupMacroArgs {
  my ($macros) = @_;

  my $macroname;

  foreach $macroname (keys %$macros) {
    my $m = $macros->{$macroname};
    my $declaration = $m->{declaration};

    if ($declaration =~ m/^\s*#\s*define\s+\w+\(([^\)]*)\)/) {
      my @params = ();
      my ($param);
      foreach $param (split (/,/, $1)) {
        $param =~ s/^\s+//;
        $param =~ s/\s*$//;
        if ($param =~ m/\S/) {
          push @params, $param;
        }
      }
      $m->{'args'} = \@params;
    }
  }
}

# generate list of arguments for the functions
sub FixupFunctionArgs {
  my ($funcs) = @_;

  my $funcname;
  foreach $funcname (keys %$funcs) {

    my $f = $funcs->{$funcname};

    my $declaration = $f->{unparsed_args};
    my $param_num = 0;
    my @args = ();

    if (! defined($funcname)) {
      print "WARNING: function with no name?\n";
      return;
    }

    if (! defined($declaration)) {
      print "WARNING: no argument list for function $funcname\n";
      return;
    }

    # strip newlines
    $declaration =~ s/\n//g;
    # newline at the end
    $declaration .= "\n";

    while ($declaration ne "") {
      if ($declaration =~ s/^[\s,]+//) {
        # skip whitespace and commas
        next;

      } elsif ($declaration =~ s/^void\s*[,\n]//) {
        if ($param_num != 0) {
          print "WARNING: void used as parameter in function $funcname\n";
        }

      } elsif ($declaration =~ s/^...\s*[,\n]//) {
        # Var args

        push @args, ["varargs", ""];

      } elsif ($declaration =~ s/^((const\s+|unsigned\s+)*(struct\s+)?(\w+)\s*(\**)\s*(const\s+)?(\**)?)\s*(\w+)?\s*(\[\d*\])?\s*[,\n]//) {
        # Normal parameter

        my $type;
        if (defined ($1)) {
          $type = $1;
          if (defined ($9)) {
            # [] array brackets after variable name, add a *
            $type .= "*";
          }
        } else {
          print "WARNING: parse failed on type for arg $param_num of $funcname\n";
          $type = "FIXMEFIXMEPARSEERROR";
        }

        my $name;
        if (defined ($8)) {
          $name = $8;
        } else {
          $name = "arg" . ($param_num + 1);
        }

        CleanupTypename(\$type);

        push @args, [$type, $name];

      } elsif ($declaration =~ s/^(const\s+|unsigned\s+)*(struct\s+)?(\w+)\s*(\**)\s*(const\s+)?\(\s*\*\s*(\w+)\s*\)\s*\(([^)]*)\)\s*[,\n]//) {
        # non-typedef'd function parameter

        my $name;
        $name = $6;
        push @args, ["TYPEFIXME", $name];

        # need to get a type here
        print "WARNING: Parsing non-typedef'd function pointers as arguments to functions does not work, FIXME\n"

      } else {
        print "WARNING: Can't parse args for function $funcname: $declaration\n";
        last;
      }
      $param_num++;
    }

    #    print "found $param_num args\n";

    $f->{args} = \@args;
  }
}

#############################################################################
# Function    : ParseEnumDeclaration
# Description : This function takes a enumeration declaration and
#               breaks it into individual enum member declarations.
# Arguments   : $declaration - the declaration to parse
#############################################################################

sub ParseEnumDeclaration {
  my ($enum, $declaration) = @_;

  # Remove comments, except magic comments starting /*<
  $declaration =~ s@/\*([^<*][^*]*|\*(?!/))*\*/@ @g;

  my @result = ();

  # We special-case GdkCursorType since it
  # is a pain to parse and will never change
  if ($enum->{name} eq 'GdkCursorType') {
    my @lines = split (/^/m, &GdkCursorValues());
    my $line;
    foreach $line (@lines) {
      if ($line =~ /(\s*[A-Za-z0-9_]+)\s*=.*/) {
        push @result, $1;
      } else {
        print "WARNING: didn't understand cursor type: $line\n";
      }
    }
    return @result;
  }

  if ($declaration =~ /^\s*$/) {
    return @result;
  }

  my $magiccommentmatch = '\/\*<\s*([A-Za-z_=-]*)\s*>\*\/';

  $enum->{is_flags} = 0;

  # Prime match after "typedef enum {" declaration
  if (!scalar($declaration =~ m/typedef\s+enum\s*($magiccommentmatch)?\s*\{(.*)/msg)) {
    die "Enum declaration '$declaration' does not begin with typedef enum {\n";
  } else {
    if (defined($2)) {
      if ($2 eq "flags") {
        $enum->{is_flags} = 1;
      }
    }
    if (defined($3)) {
      $declaration = $3;
    } else {
      print "WARNING: couldn't strip 'typedef enum {' from enum decl\n"
    }
  }

  if ($declaration =~ /<</) {
    $enum->{is_flags} = 1;
  }

  $enum->{nicks} = ();

  my @lines = split (/[,\}]/m, $declaration);

  # Treat lines in sequence.
  my $prev_elem;
  my $wholeline;
  foreach $wholeline (@lines) { # $declaration =~ m/\s*([^,\}]+)([,\}])/msg) {
    my $nick;
    my $elem;
    my $line;

    if ($wholeline =~ /$magiccommentmatch/) {
      if ($1 =~ /nick=([A-Za-z_-]+)/) {
        $nick = $1;
        if (defined($prev_elem)) {
          $enum->{nicks}->{$prev_elem} = $nick;
          print "nick `$nick' attached to $prev_elem\n";
        } else {
          print "WARNING: nick `$nick' not associated\n";
        }
      }
    }

    $line = $wholeline;

    # nuke whitespace
    chomp $line;
    $line =~ s/\s//g;

    if ($line =~ m/[A-Za-z]+;/msg) {
      ; # enumeration name, ignore
    } elsif ($line =~ m/^(\w+)\s*(=.*)?$/msg) {
      $elem = $1;
      push @result, $elem;

      # Special case for GIOCondition, where the values are specified by
      # macros which expand to include the equal sign like '=1'.
    } elsif ($line =~ m/^(\w+)\s*GLIB_SYSDEF_POLL/msg) {
      $elem = $1;
      push @result, $elem;

      # Special case include of <gdk/gdkcursors.h>, just ignore it
    } elsif ($line =~ m/^#include/) {
      last;

    } elsif ($line =~ m/\s*/) {
      ; # ignore blanks
    } else {
      warn "Cannot parse enumeration member `$line'";
    }

    $prev_elem = $elem;
  }

  return @result;
}

##############################################################################
###########  Spew routines
##############################################################################

sub SpewVariables {
    my ($vars) = @_;

    my $key;
    foreach $key (sort keys %$vars) {
        my $v = $vars->{$key};
        my $name = $v->{name};
        my $type = $v->{type};
        my $decl = $v->{declaration};

        print "VARIABLE\n";
        print "name: $name ";
        if ($type) {
          print "type: $type";
        }
        print "\n";
        if ($decl) {
          print "decl: $decl\n";
        }
    }
}

sub SpewUserFunctions {
    my ($funcs) = @_;

    my $key;
    foreach $key (sort keys %$funcs) {
        my $f = $funcs->{$key};
        my $name = $f->{name};
        my $returns = $f->{returns};
        my $args = $f->{args};
        my $decl = $f->{declaration};

        print "USER FUNCTION\n";
        print "name: $name ";
        if ($returns) {
          print "returns: $returns ";
        }
        if ($args) {
          print "args: $args ";
        }
        print "\n";
        if ($decl) {
          print "decl: $decl\n";
        }
    }
}

sub SpewUnions {
    my ($unions) = @_;

    my $key;
    foreach $key (sort keys %$unions) {
        my $u = $unions->{$key};
        my $name = $u->{name};
        my $decl = $u->{declaration};

        print "UNION\n";
        print "name: $name ";
        print "\n";
        if ($decl) {
          print "decl: $decl\n";
        }
    }
}

sub SpewObjects {
    my ($objects) = @_;

    my $key;
    foreach $key (sort keys %$objects) {
        my $o = $objects->{$key};
        my $name = $o->{name};
        my $decl = $o->{declaration};
        my $fields = $o->{fields};
        my $parent = $o->{parent};
        my $signals = $o->{signals};
        my $args = $o->{args};

        print "OBJECT\n";
        print "name: $name ";
        if ($parent) {
          print "parent: $parent ";
        }
        print "\n";
        if ($fields) {
          print "fields: ";
          my $f;
          foreach $f (@$fields) {
            print "(" . $f->[0] . ", " . $f->[1] . ") ";
          }
          print "\n";
        }

        print "signals: \n";
        my $s;
        foreach $s (@$signals) {
          print "  " . $s->{name} . "\n";
          print "  returns: " . $s->{returns} . "\n";
          print "  args:\n";
          my $sigargs = $s->{args};
          my $arg;
          foreach $arg (@$sigargs) {
            print "    " . $arg->[0] . " " . $arg->[1] . "\n";
          }
        }

        print "args: \n";
        my $a;
        foreach $a (@$args) {
          print "  " . $a->{name} . "  ". $a->{type} . "  " . $a->{flags} . "\n";
        }

        if ($decl) {
          print "decl: $decl\n";
        }
    }
}

sub SpewTypedefs {
    my ($typedefs) = @_;

    my $key;
    foreach $key (sort keys %$typedefs) {
        my $t = $typedefs->{$key};
        my $name = $t->{name};
        my $orig = $t->{orig};
        my $decl = $t->{declaration};

        print "TYPEDEF\n";
        print "name: $name ";
        if ($orig) {
          print "orig: $orig";
        }
        print "\n";
        if ($decl) {
          print "decl: $decl\n";
        }
    }
}

sub SpewMacros {
    my ($macros) = @_;

    my $key;
    foreach $key (sort keys %$macros) {
        my $m = $macros->{$key};
        my $name = $m->{name};
        my $returns = $m->{returns};
        my $args = $m->{args};
        my $decl = $m->{declaration};

        print "MACRO\n";
        print "name: $name ";
        if ($returns) {
          print "returns: $returns ";
        }
        if ($args) {
          print "args: ";
          my $a;
          foreach $a (@$args) {
            print $a;
            print " ";
          }
        }
        print "\n";
        if ($decl) {
          print "decl: $decl\n";
        }
    }
}

sub SpewEnums {
    my ($enums) = @_;

    my $key;
    foreach $key (sort keys %$enums) {
        my $e = $enums->{$key};
        my $name = $e->{name};
        my $elems = $e->{elems};
        my $decl = $e->{declaration};

        print "ENUM\n";
        print "name: $name\n";
        print "elems: ";
        my $el;
        for $el (@$elems) {
          print $el;
          print " ";
        }
        print "\n";
        if ($decl) {
          print "decl: $decl\n";
        }
    }
}

sub SpewFunctions {
    my ($funcs) = @_;

    my $key;
    foreach $key (sort keys %$funcs) {
        my $f = $funcs->{$key};
        my $name = $f->{name};
        my $returns = $f->{returns};
        my $unparsed_args = $f->{unparsed_args};
        my $args = $f->{args};
        my $decl = $f->{declaration};

        print "FUNCTION\n";
        print "name: $name ";
        if ($returns) {
          print "returns: $returns ";
        }
        print "\n";
        if ($args) {
          print "args: (\n";
          my $a;
          foreach $a (@$args) {
            print "   " . $a->[0] . " " . $a->[1] . "\n";
          }
          print ")\n";
        }
        if ($decl) {
          print "decl: $decl\n";
        }
        if ($unparsed_args) {
          print "raw args: $unparsed_args\n";
        }
    }
}

sub SpewStructs {
    my ($structs) = @_;

    my $key;
    foreach $key (sort keys %$structs) {
        my $s = $structs->{$key};
        my $name = $s->{name};
        my $decl = $s->{declaration};

        print "STRUCT\n";
        print "name: $name\n";
        if ($decl) {
          print "decl: $decl\n";
        }
    }
}


#############################################################################
# Function    : ScanHeaders
# Description : This scans a directory tree looking for header files.
#
# Arguments   : $source_dir - the directory to scan.
#############################################################################

sub ScanHeaders {
    my ($self, $source_dir, $ignore_headers) = @_;
#    print "Scanning source directory: $source_dir\n";

    # This array holds any subdirectories found.
    my (@subdirs) = ();

    opendir (SRCDIR, $source_dir)
	|| die "Can't open source directory $source_dir: $!";
    my $file;
    foreach $file (readdir (SRCDIR)) {
	if ($file eq '.' || $file eq '..') {
	    next;
	} elsif (-d "$source_dir/$file") {
	    push (@subdirs, $file);
	} elsif ($file =~ m/\.h$/) {
	    &ScanHeader ($self, "$source_dir/$file", $ignore_headers);
	}
    }
    closedir (SRCDIR);

    # Now recursively scan the subdirectories.
    my $dir;
    foreach $dir (@subdirs) {
	&ScanHeaders ($self, "$source_dir/$dir", $ignore_headers);
    }
}


#############################################################################
# Function    : ScanHeader
# Description : This scans a header file, looking for declarations of
#		functions, macros, typedefs, structs and unions, which it
#               stuffs in our hashes
# Arguments   : $input_file - the header file to scan.
#############################################################################

sub ScanHeader {
  my ($self, $input_file, $ignore_headers) = @_;
  #    print "DEBUG: Scanning $input_file\n";

  my $list = "";		# Holds the resulting list of declarations.
  my ($in_comment) = 0;         # True if we are in a comment.
  my ($in_declaration) = "";	# The type of declaration we are in, e.g.
				#   'function' or 'macro'.
  my ($symbol);                 # The current symbol being declared.
  my ($decl);			# Holds the declaration of the current symbol.
  my ($ret_type);		# For functions and function typedefs this
				#   holds the function's return type.
  my ($previous_line) = "";	# The previous line read in - some Gnome
 				#   functions have the return type on one line
				#   and the rest of the declaration after.
  my ($first_macro) = 1;	# Used to try to skip the standard #ifdef XXX
				#   #define XXX at the start of headers.
  my ($level);                  # Used to handle structs which contain nested
                                #   structs or unions.

  my $file_basename;
  if ($input_file =~ m/^.*[\/\\](.*)\.h$/) {
    $file_basename = $1;
  } else {
    print "WARNING: Can't find basename of file $input_file\n";
    $file_basename = $input_file;
  }

  # Check if the basename is in the list of headers to ignore.
  if ($ignore_headers =~ m/\b\Q${file_basename}\E\.h\b/) {
    #	print "DEBUG: File ignored: $input_file\n";
    return;
  }

  if (! -f $input_file) {
    print "File doesn't exist: $input_file\n";
    return;
  }

  open(INPUT, $input_file)
    || die "Can't open $input_file: $!";
  while (<INPUT>) {
    # If this is a private header, skip it.
    if (m%/*<\s*private_header\s*>*/%) {
      last;
    }

    # Skip to the end of the current comment.
    if ($in_comment) {
      #	    print "Comment: $_";
      if (m%\*/%) {
        $in_comment = 0;
      }
      next;
    }

    if (!$in_declaration) {
      # Skip top-level comments.
      if (s%^\s*/\*%%) {
        if (m%\*/%) {
          #		    print "Found one-line comment: $_";
        } else {
          $in_comment = 1;
          #		    print "Found start of comment: $_";
        }
        next;
      }

      # MACROS

      if (m/^\s*#\s*define\s+(\w+)/) {
        $symbol = $1;
        # We assume all macros which start with '_' are private, but
        # we accept '_' itself which is the standard gettext macro.
        # We also try to skip the first macro if it looks like the
        # standard #ifndef HEADER_FILE #define HEADER_FILE etc.
        # And we only want TRUE & FALSE defined in GLib (libdefs.h in
        # libgnome also defines them if they are not already defined).
        if (($symbol !~ m/^_/
             && ($previous_line !~ m/#ifndef\s+$symbol/
                 || $first_macro == 0)
             && (($symbol ne 'TRUE' && $symbol ne 'FALSE')
                 || $self->{module} eq 'glib'))
            || $symbol eq "_") {
          $decl = $_;
          $in_declaration = "macro";
        }
        $first_macro = 0;


        # TYPEDEF'D FUNCTIONS (i.e. user functions)

      } elsif (m/^\s*typedef\s+((const\s+)?\w+)\s*(\**)\s*\(\*\s*(\w+)\)\s*\(/) {
        $ret_type = "$1 $3";
        $symbol = $4;
        $decl = $';
        $in_declaration = "user_function";


        # ENUMS

      } elsif (s/^\s*enum\s+_(\w+)\s+\{/enum $1 {/) {
        # We assume that 'enum _<enum_name> {' is really the
        # declaration of enum <enum_name>.
        $symbol = $1;
        #		print "DEBUG: plain enum: $symbol\n";
        $decl = $_;
        $in_declaration = "enum";

      } elsif (m/^\s*typedef\s+enum\s+_(\w+)\s+\1\s*;/) {
        # We skip 'typedef <enum_name> _<enum_name>;' as the enum will
        # be declared elsewhere.
        #		print "DEBUG: skipping enum typedef: $1\n";

      } elsif (m/^\s*typedef\s+enum/) {
        $symbol = "";
        $decl = $_;
        $in_declaration = "enum";


        #STRUCTS

      } elsif (m/^\s*typedef\s+struct\s+_(\w+)\s+\1\s*;/) {
        # We've found a 'typedef struct _<name> <name>;'
        # This could be an opaque data structure, so we output an
        # empty declaration. If the structure is actually found that
        # will override this.
        #		print "DEBUG: struct typedef: $1\n";
        &AddSymbolToList (\$list, $1);
        my (%struct) = ('name' => $1,
                        'declaration' => $_);
        $self->{structs}->{$1} = \%struct;

      } elsif (m/^\s*typedef\s+union\s+_(\w+)\s+\1\s*;/) {
        # We've found a 'typedef union _<name> <name>;'
        # This could be an opaque data structure, so we output an
        # empty declaration. If the structure is actually found that
        # will override this.
        #		print "DEBUG: struct typedef: $1\n";
        &AddSymbolToList (\$list, $1);
        my (%union) = ('name' => $1,
                        'declaration' => $_);
        $self->{unions}->{$1} = \%union;

      } elsif (m/^\s*typedef\s+struct\s*{/) {
        $symbol = "";
        $decl = $_;
        $level = 0;
        $in_declaration = "struct";

        #	    } elsif (m/^\s*struct\s+_(\w+)\s*;/) {
        # Skip private structs.
        # don't skip these, they aren't private...

      } elsif (s/^\s*struct\s+_(\w+)/struct $1/) {
        # We assume that 'struct _<struct_name>' is really the
        # declaration of struct <struct_name>.
        $symbol = $1;
        $decl = $_;
        $level = 0;
        $in_declaration = "struct";


        # UNIONS

      } elsif (s/^\s*union\s+_(\w+)/union $1/) {
        $symbol = $1;
        $decl = $_;
        $in_declaration = "union";

      } elsif (m/^\s*(G_GNUC_EXTENSION\s+)?typedef\s+(.+[\s\*])(\w\S*);/) {
        if ($2 !~ m/^struct\s/ && $2 !~ m/^union\s/) {
          #		    print "Found typedef: $_";
          &AddSymbolToList (\$list, $3);
          my (%typedef) = ('name' => $3,
                           'original' => $2,
                           'declaration' => $_);
          $self->{typedefs}->{$3} = \%typedef;
        }
      } elsif (m/^\s*typedef\s+/) {
        #		print "Skipping typedef: $_";


        # VARIABLES (extern'ed variables)

      } elsif (m/^\s*extern\s+((const\s+|unsigned\s+)*\w+)(\s+\*+|\*+|\s)\s*([A-Za-z]\w*)\s*;/) {
        $symbol = $4;
        #		print "Possible extern: $_";
        &AddSymbolToList (\$list, $symbol);
        my (%variable) = ('name' => $symbol,
                          'type' => "$1$3",
                          'declaration' => $_ );
        $self->{variables}->{$symbol} = \%variable;

        # FUNCTIONS

        # We assume that functions which start with '_' are private, so
        # we skip them.
      } elsif (m/^\s*(extern)?\s*(G_INLINE_FUNC)?\s*((const\s+|unsigned\s+)*\w+)(\s+\*+|\*+|\s)\s*([A-Za-z]\w*)\s*\(/) {
        $ret_type = "$3 $5";
        $symbol = $6;
        $decl = $';

        if ($1) {
          #		    print "Function: $symbol, Returns: $ret_type\n";
        }
        $in_declaration = "function";

        # Try to catch function declarations which have the return type on
        # the previous line. But we don't want to catch complete functions
        # which have been declared G_INLINE_FUNC, e.g. g_bit_nth_lsf in
        # glib, or 'static inline' functions.
      } elsif (m/^\s*([A-Za-z]\w*)\s*\(/) {
        $symbol = $1;
        $decl = $';
        if ($previous_line !~ m/^\s*G_INLINE_FUNC/
            && $previous_line !~ m/^\s*static\s+/) {
          if ($previous_line =~ m/^\s*(extern)?\s*((const\s*)?\w+)(\s*\*+)?\s*$/) {
            $ret_type = $2;
            if (defined ($4)) {
              $ret_type .= " $4";
            }
            #			print "Function: $symbol, Returns: $ret_type\n";
            $in_declaration = "function";
          }
        }

        # Try to catch function declarations with the return type and name
        # on the previous line, and the start of the parameters on this.
      } elsif (m/^\s*\(/) {
        $decl = $';
        if ($previous_line =~ m/^\s*(extern)?\s*(G_INLINE_FUNC)?\s*((const\s+|unsigned\s+)*\w+)(\s+\*+|\*+|\s)\s*([A-Za-z]\w*)\s*$/) {
          $ret_type = "$3 $5";
          $symbol = $6;
          #		    print "Function: $symbol, Returns: $ret_type\n";
          $in_declaration = "function";
        }

        #	    } elsif (m/^extern\s+/) {
        #		print "Skipping extern: $_";

      }
    } else {
      # If we were already in the middle of a declaration, we simply add
      # the current line onto the end of it.
      $decl .= $_;
    }

    # Note that sometimes functions end in ') G_GNUC_PRINTF (2, 3);'.
    if ($in_declaration eq 'function') {
      if ($decl =~ s/\)\s*(G_GNUC_.*)?;.*$//) {
        $decl =~ s%/\*.*?\*/%%gs; # remove comments.
        #		$decl =~ s/^\s+//;		# remove leading whitespace.
        #		$decl =~ s/\s+$//;		# remove trailing whitespace.
        $decl =~ s/\s*\n\s*//g; # remove whitespace at start
        # and end of lines.
        $ret_type =~ s%/\*.*?\*/%%g; # remove comments in ret type.
        CleanupTypename(\$ret_type); # cleanup ret type otherwise

        &AddSymbolToList (\$list, $symbol);
        my (%function) = ('name' => $symbol,
                          'returns' => $ret_type,
                          'unparsed_args' => $decl);
        $self->{functions}->{$symbol} = \%function;

        $in_declaration = "";
      }
    }

    if ($in_declaration eq 'user_function') {
      if ($decl =~ s/\).*$//) {
        &AddSymbolToList (\$list, $symbol);
        $ret_type =~ s%/\*.*?\*/%%g; # remove comments in ret type.
        CleanupTypename(\$ret_type);
        my (%user_function) = ('name' => $symbol,
                               'returns' => $ret_type,
                               'unparsed_args' => $decl );
        $self->{user_functions}->{$symbol} = \%user_function;
        $in_declaration = "";
      }
    }

    if ($in_declaration eq 'macro') {
      if ($decl !~ m/\\\s*$/) {
        &AddSymbolToList (\$list, $symbol);
        my (%macro) = ('name' => $symbol,
                       'declaration' => $decl );
        $self->{macros}->{$symbol} = \%macro;
        $in_declaration = "";
      }
    }

    if ($in_declaration eq 'enum') {
      if ($decl =~ m/\}\s*(\w+)?;\s*$/) {
        if ($symbol eq "") {
          $symbol = $1;
        }
        &AddSymbolToList (\$list, $symbol);
        my (%enum) = ('name' => $symbol,
                      'declaration' => $decl);
        $self->{enums}->{$symbol} = \%enum;
        $in_declaration = "";
      }
    }

    # We try to handle nested stucts/unions, but unmatched brackets in
    # comments will cause problems.
    if ($in_declaration eq 'struct') {
      if ($level <= 1 && $decl =~ m/\}\s*(\w*);\s*$/) {
        if ($symbol eq "") {
          $symbol = $1;
        }

        if ($symbol =~ m/^(\S+)Class/
            && $1 ne 'GtkStyle'
            && $1 ne 'GtkType') {
          #		    print "Found object: $1\n";
          $list .= "<TITLE>$1</TITLE>\n";
          my (%object) = ('name' => $1);
          $self->{objects}->{$1} = \%object;
        } else {
          &AddSymbolToList (\$list, $symbol);

          if ($decl =~ m/^\s*typedef\s+struct\s*{/) {
            my (%struct) = ('name' => $symbol,
                            'declaration' => $decl);
            $self->{structs}->{$symbol} = \%struct;
          } else {
            my (%typedef) = ('name' => $symbol,
                             'declaration' => $decl);
            $self->{typedefs}->{$symbol} = \%typedef;
          }
        }
        $in_declaration = "";
      } else {
        # We use tr to count the brackets in the line, and adjust
        # $level accordingly.
        $level += tr/{//;
        $level -= tr/}//;
      }
    }

    if ($in_declaration eq 'union') {
      if ($decl =~ m/\}\s*;\s*$/) {
        &AddSymbolToList (\$list, $symbol);
        my (%union) = ('name' => $symbol,
                       'declaration' => $decl);
        $self->{unions}->{$symbol} = \%union;
        $in_declaration = "";
      }
    }

    $previous_line = $_;
  }
  close(INPUT);
}


#############################################################################
# Function    : AddSymbolToList
# Description : This adds the symbol to the list of declarations, but only if
#		it is not already in the list.
# Arguments   : $list - reference to the list of symbols, one on each line.
#		$symbol - the symbol to add to the list.
#############################################################################

sub AddSymbolToList {
    my ($list, $symbol) = @_;

    if ($$list =~ m/\b\Q$symbol\E\b/) {
#	print "Symbol $symbol already in list. skipping\n";
	return;
    }
    $$list .= "$symbol\n";
}

#############################################################################
# GtkObject scanner (compile a C program to spew object signals, args,
#  and hierarchy)
#############################################################################

sub QueryObjectRuntime {
  my ($types_file, $no_gtk_init, $add_gnome_init, $objs, $module) = @_;

  open (TYPES, $types_file) || die "Cannot open $types_file: $!\n";

  my $includes = "#include <gtk/gtk.h>\n";
  my @types = ();

  for (<TYPES>) {
    if (/^#include/) {
      $includes .= $_;
    } elsif (/^%/) {
      next;
    } elsif (/^\s*$/) {
      next;
    } else {
      chomp;
      push @types, $_;
    }
  }

  my $ntypes = @types + 1;

  close TYPES;

  open (TEMPL, "./objscanner-tmpl.c") || die "Cannot open objscanner-tmpl.c: $!\n";

  open (OUTFILE, ">./objscanner.c") || die "Cannout open ./objscanner.c for output: $!\n";

  for (<TEMPL>) {
    if (/\/\*@(\w+)@\*\//) {
      # insert stuff
      if ($1 eq "includes") {
        print OUTFILE $includes;
      } elsif ($1 eq "typesarray") {
        print OUTFILE "GtkType object_types[$ntypes];\n";
      } elsif ($1 eq "typeelements") {
        for (@types) {
          print OUTFILE "    object_types[i++] = $_ ();\n";
        }
      } elsif ($1 eq "init") {
        if ($no_gtk_init) {
          print OUTFILE "gtk_type_init ();\n";
        } else {
          print OUTFILE "gtk_init (&argc, &argv);\n";
        }
        if ($add_gnome_init) {
          print OUTFILE "gnome_type_init();\n";
        }
      } else {
        die "weird template\n";
      }
    } else {
      print OUTFILE $_;
    }
  }

  close TEMPL;
  close OUTFILE;

  # Compile and run our file

  my $CC = $ENV{CC} ? $ENV{CC} : "gcc";
  my $CFLAGS = $ENV{CFLAGS} ? $ENV{CFLAGS} : "";
  my $LDFLAGS = $ENV{LDFLAGS} ? $ENV{LDFLAGS} : "";

  my $command = "$CC $CFLAGS -o objscanner objscanner.c $LDFLAGS >/dev/null";

  print $command . "\n";
  system($command) == 0 or die "Compilation of scanner failed\n";

  open (SCANNER, "./objscanner|") || die "can't run scanner command";

  # initialize list elements
  my $oname;
  foreach $oname (keys %$objs) {
    my $o = $objs->{$oname};

    $o->{signals} = [];
    $o->{args} = [];
  }

  my $signal;
  my $arg;
  my $owner;
  my $argtype;

  for (<SCANNER>) {
    if (/^parent (\w+) (\w+)$/) {

      my $objname = $2;
      my $obj = $objs->{$objname};

      $obj->{parent} = $1;

    } elsif (/^signal ([-A-Za-z0-9_]+) of (\w+)$/) {

      $owner = $2;
      $signal = {'name' => $1};
      $signal->{args} = [];

    } elsif (/^returns (.*)$/) {

      if (defined($signal)) {
        $signal->{returns} = $1;
      } else {
        print "WARNING: no signal for returns field\n";
      }

    } elsif (/^endsignal$/) {

      if (!defined($signal)) {
        print "WARNING: no signal to end?\n";
      }

      my $obj = $objs->{$owner};

      if (!defined($obj)) {
        print "WARNING: don't know about object $owner\n";
      } else {
        my $sigs = $obj->{signals};
        if (!defined($sigs)) {
          print "WARNING: no signal list defined for " . $obj->{name} . "\n";
        }
        push @$sigs, $signal;
      }

      undef $owner;
      undef $signal;
      undef $argtype;

    } elsif (/^argtype ([-*A-Za-z0-9_:]+)$/) {

      if (defined($signal)) {
        $argtype = $1;
      } elsif (defined($arg)) {
        $arg->{type} = $1;
      } else {
        print "WARNING: no arg or signal for argtype\n";
      }

    } elsif (/^argname ([-A-Za-z0-9_:]+)$/) {


      if (defined($signal)) {
        my $args = $signal->{args};
        push @$args, [$argtype, $1];
      } elsif (defined($arg)) {
        # strip out the scope
        my $name = $1;
        $name =~ s/[^:]+::([-A-Za-z0-9_:]+)/$1/;
        $arg->{name} = $name;
      } else {
        print "WARNING: argname not associated with anything\n";
      }

    } elsif (/^arg of (\w+)$/) {

      $owner = $1;
      $arg = {};

    } elsif (/^argflags ([rwxXc]+)$/) {

      if (defined($arg)) {
        $arg->{flags} = $1;
      } else {
        print "WARNING: no arg for flags\n";
      }

    } elsif (/^endarg$/) {

      if (!defined($arg)) {
        print "WARNING: no signal to end?\n";
      }

      my $obj = $objs->{$owner};

      if (!defined($obj)) {
        print "WARNING: don't know about object $owner\n";
      } else {
        my $args = $obj->{args};

        if (!defined($args)) {
          print "WARNING: no arg list defined for " . $obj->{name} . "\n";
        }

        push @$args, $arg;
      }

      undef $owner;
      undef $arg;
      undef $argtype;

    } else {
      print "WARNING: didn't understand objscanner output: '$_'\n";
    }
  }

  close SCANNER;

  unlink "./objscanner.c";

  # debugging
  system("mv ./objscanner ./objscanner.$module");
  # end debugging

  unlink "./objscanner";
}


####### Inline this

sub GdkCursorValues {
  my $gdkcursor_values;

  $gdkcursor_values = <<EOT
GDK_NUM_GLYPHS = 154,
GDK_X_CURSOR = 0,
GDK_ARROW = 2,
GDK_BASED_ARROW_DOWN = 4,
GDK_BASED_ARROW_UP = 6,
GDK_BOAT = 8,
GDK_BOGOSITY = 10,
GDK_BOTTOM_LEFT_CORNER = 12,
GDK_BOTTOM_RIGHT_CORNER = 14,
GDK_BOTTOM_SIDE = 16,
GDK_BOTTOM_TEE = 18,
GDK_BOX_SPIRAL = 20,
GDK_CENTER_PTR = 22,
GDK_CIRCLE = 24,
GDK_CLOCK = 26,
GDK_COFFEE_MUG = 28,
GDK_CROSS = 30,
GDK_CROSS_REVERSE = 32,
GDK_CROSSHAIR = 34,
GDK_DIAMOND_CROSS = 36,
GDK_DOT = 38,
GDK_DOTBOX = 40,
GDK_DOUBLE_ARROW = 42,
GDK_DRAFT_LARGE = 44,
GDK_DRAFT_SMALL = 46,
GDK_DRAPED_BOX = 48,
GDK_EXCHANGE = 50,
GDK_FLEUR = 52,
GDK_GOBBLER = 54,
GDK_GUMBY = 56,
GDK_HAND1 = 58,
GDK_HAND2 = 60,
GDK_HEART = 62,
GDK_ICON = 64,
GDK_IRON_CROSS = 66,
GDK_LEFT_PTR = 68,
GDK_LEFT_SIDE = 70,
GDK_LEFT_TEE = 72,
GDK_LEFTBUTTON = 74,
GDK_LL_ANGLE = 76,
GDK_LR_ANGLE = 78,
GDK_MAN = 80,
GDK_MIDDLEBUTTON = 82,
GDK_MOUSE = 84,
GDK_PENCIL = 86,
GDK_PIRATE = 88,
GDK_PLUS = 90,
GDK_QUESTION_ARROW = 92,
GDK_RIGHT_PTR = 94,
GDK_RIGHT_SIDE = 96,
GDK_RIGHT_TEE = 98,
GDK_RIGHTBUTTON = 100,
GDK_RTL_LOGO = 102,
GDK_SAILBOAT = 104,
GDK_SB_DOWN_ARROW = 106,
GDK_SB_H_DOUBLE_ARROW = 108,
GDK_SB_LEFT_ARROW = 110,
GDK_SB_RIGHT_ARROW = 112,
GDK_SB_UP_ARROW = 114,
GDK_SB_V_DOUBLE_ARROW = 116,
GDK_SHUTTLE = 118,
GDK_SIZING = 120,
GDK_SPIDER = 122,
GDK_SPRAYCAN = 124,
GDK_STAR = 126,
GDK_TARGET = 128,
GDK_TCROSS = 130,
GDK_TOP_LEFT_ARROW = 132,
GDK_TOP_LEFT_CORNER = 134,
GDK_TOP_RIGHT_CORNER = 136,
GDK_TOP_SIDE = 138,
GDK_TOP_TEE = 140,
GDK_TREK = 142,
GDK_UL_ANGLE = 144,
GDK_UMBRELLA = 146,
GDK_UR_ANGLE = 148,
GDK_WATCH = 150,
GDK_XTERM = 152,
EOT
;

    return $gdkcursor_values;
}
